export const pages = {
  Home: 'home',
  AllPosts: 'allPosts',
  About: 'about',
  Contact: 'contact',
  Register: 'register',
  Login: 'login',
};
