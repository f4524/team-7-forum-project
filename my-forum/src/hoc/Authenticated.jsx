import {useContext} from 'react';
import AppContext from '../data/app-state';
import {Navigate} from 'react-router-dom';

// eslint-disable-next-line require-jsdoc
export default function Authenticated({children}) {
  const {user} = useContext(AppContext);
  if (!user) {
    return <Navigate to="/home" />;
  }

  return children;
}
