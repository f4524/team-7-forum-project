import {initializeApp} from 'firebase/app';
import {getAuth} from 'firebase/auth';
import {getDatabase} from 'firebase/database';
import {getStorage} from 'firebase/storage';

const firebaseConfig = {
  apiKey: 'AIzaSyAdJz3TMB1AIyB32yEQhIdSGMJdJ1PiPes',
  authDomain: 'forum-team7.firebaseapp.com',
  projectId: 'forum-team7',
  storageBucket: 'gs://forum-team7.appspot.com',
  messagingSenderId: '651059729262',
  appId: '1:651059729262:web:a7bc06f677bc758ecf277f',
  measurementId: 'G-6T0T706T70',
  databaseURL: 'https://forum-team7-default-rtdb.europe-west1.firebasedatabase.app/',
};

export const app = initializeApp(firebaseConfig);
// the Firebase authentication handler
export const auth = getAuth(app);
// the Realtime Database handler
export const db = getDatabase(app);
export const storage = getStorage(app);
