/* eslint-disable require-jsdoc */
import {VscSearch} from 'react-icons/vsc';
import './Searchbar.css';
import {useState} from 'react';
import {NavLink} from 'react-router-dom';

function Search() {
  const [searchField, setSearchField] = useState('');

  const handleChange = (e) => {
    setSearchField(e.target.value);
  };

  const deleteSearchText = () => {
    const searchField = document.getElementById('search-field');
    searchField.value = '';
  };

  return (
    <div className="search-section">
      <input
        className="search-bar"
        type="search"
        name="search-form"
        placeholder="Search here..."
        id="search-field"
        onChange={handleChange}/>

      <NavLink
        to={`/search?searchId=${searchField}`}
        onClick={deleteSearchText}>
        <VscSearch className="search-button" />
      </NavLink>
    </div>
  );
};

export default Search;
