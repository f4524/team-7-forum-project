/* eslint-disable max-len */
/* eslint-disable valid-jsdoc */
import {useContext, useEffect, useState} from 'react';
import AppContext from '../../../data/app-state';
import {Avatar} from '@mui/material';
import {AiOutlineEdit} from 'react-icons/ai';
import {FcLikePlaceholder} from 'react-icons/fc';
import {FcLike} from 'react-icons/fc';
import {MdDeleteOutline} from 'react-icons/md';
import {updateComment} from '../../../services/comments.service';
import './Comment.css';
import {AiOutlineSave} from 'react-icons/ai';
import {getUserByUsername} from '../../../services/users.service';

/**
 * @param {{ post: { id: string, content: string, author: string, date: Date, likedBy: string[] } } } post
 * @returns
 */
const Comment = ({deleteComment, comment, like, dislike}) => {
  const {userData} = useContext(AppContext);
  const [user, setUser] = useState();
  const [editContent, setEditContent] = useState();
  const [bool, setBool] = useState('false');
  useEffect(() => {
    (async () => {
      setUser(await (await getUserByUsername(comment?.author)).val());
    })();
  }, []);


  const isCommentLiked = () => comment.likedBy.includes(userData.username);
  const handleLikeButtonClick = () =>
    isCommentLiked() ? dislike(comment) : like(comment);

  const editComment = () => {
    if (comment !== undefined) {
      updateComment(comment.author, editContent, comment.id, comment.postId);
    }
    (() => setBool('false'))();
  };

  return (
    <>
      {user &&
       (user?.avatarUrl !== undefined ?
          <img src={user.avatarUrl} alt="profile" className='imgProfile' /> :
          <Avatar
            className="avatar-icon"
            sx={{bgcolor: '#f4976c', fontSize: 34, width: 80, height: 80}}/>
       )}

      <div className="post-content">
        <div className="content">
          <div className="authorData">
            <label className="labelAuthor">author:</label> {comment?.author}
          </div>

          <div>
            <hr />
            <hr className="second-hr" />
          </div>

          <div className="comment-text">
            {bool === 'true' ?
                <>
                  <textarea value={editContent} onChange={(e) => setEditContent(e.target.value)} className='saveButtonForEditComment'>
                    {comment?.content}
                  </textarea>
                  {() => setBool('false')}
                </> :
                comment?.content }
            <div className="post-icon-comment">
              {comment.author === userData.username ? (
                <>
                  {bool === 'true' ?
                  <AiOutlineSave className="save-icon" onClick={editComment} /> : null}
                  <AiOutlineEdit className="edit-icon" onClick={() => setBool('true')} />
                  <MdDeleteOutline
                    className="delete-icon"
                    onClick={() => deleteComment(comment.id)}/>
                </>
              ) : null}
            </div>
          </div>
        </div>

        <div className="right-section-post">
          <div className="post-like">
            <div className="post-like-nmb">
              <button onClick={handleLikeButtonClick}>
                {isCommentLiked() ? (
                  <FcLike className="post-likes" />
                ) : (
                  <FcLikePlaceholder className="post-likes" />
                )}
              </button>
              <span>
                {comment.likedBy.length} likes
              </span>
            </div>
          </div>

          <div className="authorDateTime">
            {new Date(comment.createdOn).toLocaleDateString()}
          </div>

        </div>
      </div>
    </>
  );
};

export default Comment;
