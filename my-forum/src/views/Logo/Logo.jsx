import './Logo.css';

const Logo = ({logo}) => {
  const title = 'Health and Wellness';
  return (
    <>
      <img className="logoIcon" src={logo} alt="logo"></img>
      <div className="head-title">{title}</div>
    </>
  );
};

export default Logo;
