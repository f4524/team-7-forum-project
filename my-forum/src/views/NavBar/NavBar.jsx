/* eslint-disable max-len */
import './NavBar.css';
import {NavLink} from 'react-router-dom';
import {useContext} from 'react';
import AppContext from '../../data/app-state';

const NavBar = () => {
  const {user, userData} = useContext(AppContext);

  return (
    <div className="navBar">
      <div className="menuShowMenu">
        <nav className="nav-links">
          < NavLink to="/home" >Home</NavLink>
          {user !== null ? <NavLink to="/all-posts">All posts</NavLink> : null}
          {userData !== null && userData.role === 2 ? <NavLink to="/search-user">Search user</NavLink> : null}
          < NavLink to="/about">About us</NavLink>
          < NavLink to="/contact" >Contact</NavLink>
        </nav>
      </div>
    </div>
  );
};

export default NavBar;
