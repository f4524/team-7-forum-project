import './LogInButtons.css';
import {NavLink} from 'react-router-dom';

const LogInButtons = () => {
  return (
    <>
      <div className="login-nav-links">
        <NavLink to="/register" className="register-btn" >Register</NavLink>
      </div>

      <div className="login-nav-links">
        <NavLink to="/login" className="login-btn">Login</NavLink>
      </div>
    </>
  );
};

export default LogInButtons;
