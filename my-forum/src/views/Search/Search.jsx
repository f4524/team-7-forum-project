/* eslint-disable require-jsdoc */
import './Search.css';
import {useEffect, useState} from 'react';
import {fromPostsDocument, getLivePosts} from '../../services/posts.services';
import {useSearchParams} from 'react-router-dom';
import Post from '../Post/Post';

export default function Search() {
  const [searchParam] = useSearchParams();
  const [posts, setPosts] = useState([]);
  const searchId = searchParam.get('searchId');

  useEffect(() => {
    const unsubscribe = getLivePosts((snapshot) => {
      setPosts(fromPostsDocument(snapshot));
    });

    return () => unsubscribe();
  }, []);

  const filteredPosts = posts.filter((post) => {
    return (
      post.author.toLowerCase().includes(searchId.toLowerCase()) ||
      post.title.toLowerCase().includes(searchId.toLowerCase()) ||
      post.tags
      // .toLowerCase()
          .includes(searchId.toLowerCase())
    );
  });

  return (
    <div className="Search">
      <div className="searched-posts-title">
        <h2>Searched results for {searchId}: </h2>
      </div>
      <div className="searched-posts">
        {searchId === '' || filteredPosts.length === 0 ? (
          <p> Not found !</p>
        ) : (
          filteredPosts.map((post) => (
            <div className="each-post" key={post.id}>
              <Post post={post} />
            </div>
          ))
        )}
      </div>
    </div>
  );
}
