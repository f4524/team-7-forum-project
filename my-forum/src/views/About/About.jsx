/* eslint-disable max-len */
/* eslint-disable require-jsdoc */
import './About.css';
import {BsFillCaretRightFill} from 'react-icons/bs';

export default function About() {
  return (
    <div className='About'>
      <div className="about-body">
        <h2>About the forum:</h2>
        <p>
          <BsFillCaretRightFill className='point'/>
        The Healthy and Wellness Forum took place on 18 Мay 2022 under the slogan “We have everything you need”.
        </p>
        <p>
          <BsFillCaretRightFill className='point'/>
          Healthy and Wellness has launched a new category for the platform "Small Business Offers". Advertise
          your products and services at a preferential price to dozens of users every month! For more information
          you can not call or write in the Contacts section.
        </p>
        <p>
          <BsFillCaretRightFill className='point'/>
          The use of Healthy and Wellness to share useful information is increasing every second Users are
          willing to recommend the site if they need information or advice.
        </p>
        <p>
          <BsFillCaretRightFill className='point'/>
          Healthy and Wellness is the Bulgarian online community that brings together an active audience with interests
          in healthy eating. Users do not share their forum pages every day, look for different tips and answer questions.
        </p>
        <br />
        <p>
          <BsFillCaretRightFill className='point'/>
          Partners in the right of the site:</p>
        <p>Georgi Indjev</p>
      </div>
    </div>
  );
}
