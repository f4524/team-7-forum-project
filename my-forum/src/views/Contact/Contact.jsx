/* eslint-disable max-len */
/* eslint-disable require-jsdoc */
import './Contact.css';
import {FcCallback} from 'react-icons/fc';
import {FaGoogle} from 'react-icons/fa';
import {AiFillLinkedin} from 'react-icons/ai';

export default function Contact() {
  return (
    <div className='Contact'>
      <div className="contact-body">
        <h2>Contact us:</h2>
        <div>For additional information or in case of problems with the forum, please contact us on:</div>
        <p><FcCallback/> +359 882 705 638</p>
        <p><FaGoogle className='google-icon'/>    georgiindjev98@gmail.com</p>
        {/* eslint-disable-next-line jsx-a11y/anchor-has-content */ }
        <p>
          <a href='' title='Linkedin'>
            <AiFillLinkedin/>     Linkedin profile
          </a>
        </p>
      </div>
    </div>
  );
}
