/* eslint-disable require-jsdoc */
import {useContext} from 'react';
import {addPost} from '../../services/posts.services';
import CreatePost from '../../components/CreatePost/CeatePost';
import AppContext from '../../data/app-state';

export default function NewPosts() {
  const {user, userData} = useContext(AppContext);

  const createPost = (title, content, tags) => {
    return addPost(title, content, tags, userData.username)
        .then((post) => {});
  };

  return (
    <div className='All-posts'>
      <CreatePost onSubmit={createPost} user={user} userData={userData} />
    </div>
  );
}
