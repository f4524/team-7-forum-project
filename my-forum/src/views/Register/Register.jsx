/* eslint-disable max-len */
/* eslint-disable require-jsdoc */
import {useState} from 'react';
import {useNavigate} from 'react-router-dom';
import './Register.css';
import {BsPersonCircle} from 'react-icons/bs';
import {registerUser} from '../../services/auth.service';
import {getUserByUsername} from '../../services/users.service';
import {createUserUsername} from '../../services/users.service';

const Register = () => {
  const [form, setForm] = useState({
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    username: '',
    role: '',
  });

  const navigate = useNavigate();

  const updateForm = (prop) => (e) => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };

  function containsNumber(str) {
    return /\d/.test(str);
  }

  const validation = () => {
    if (form.password.length < 8) {
      document.forms['myForm']['password'].style.border = '2px solid red';
      document.getElementById('password-msg').innerHTML =
        'Not a valid password! Must be at least 8 symbols.';
    }
    if (
      form.firstName.length < 4 ||
      form.firstName.length > 32 ||
      containsNumber(form.firstName)
    ) {
      document.forms['myForm']['firstName'].style.border = '2px solid red';
      document.getElementById('firstName-msg').innerHTML =
        'Not a valid name! Must be at least 4 symbols.';
    }
    if (
      form.lastName.length < 4 ||
      form.lastName.length > 32 ||
      containsNumber(form.lastName)
    ) {
      document.forms['myForm']['lastName'].style.border = '2px solid red';
      document.getElementById('lastName-msg').innerHTML =
        'Not a valid name! Must be at least 4 symbols.';
    }
    if (!/^\w+([.-]?\w+)@\w+([.-]?\w+)(.\w{2,3})+$/.test(form.email)) {
      document.forms['myForm']['email'].style.border = '2px solid red';
      document.getElementById('email-msg').innerHTML = 'Not a valid email!';
    }
  };

  const register = (e) => {
    e.preventDefault();

    validation();

    getUserByUsername(form.username)
        .then(async (snapshot) => {
          if (snapshot.exists()) {
            document.forms['myForm']['username'].style.border = '2px solid red';
            document.getElementById(
                'username-msg'
            ).innerHTML = `User with username @${form.username} already exists!`;
          }
          try {
            const u = await registerUser(form.email, form.password);
            createUserUsername(
                form.username,
                u.user.uid,
                form.email,
                form.password,
                form.firstName,
                form.lastName
            )
                .then(() => {
                  navigate('/home');
                })
                .catch(console.error);
          } catch (error) {
            if (error.message.includes(`email-already-in-use`)) {
              document.forms['myForm']['email'].style.border = '2px solid red';
              document.getElementById(
                  'email-msg'
              ).innerHTML = `Email ${form.email} has already been registered!`;
            }
          }
        })
        .catch(console.error);
  };

  return (
    <div className="Register">
      <div className="user-register">
        <div className="title-form">
          <h2>Register </h2>

          <div className="profile-icon">
            <BsPersonCircle className="profile-icon" />
          </div>
        </div>

        <form action="/" name="myForm" className="Form-register">
          <div className="section">
            <label htmlFor="name">First name: </label>
            <input
              id="firstName"
              type="email"
              placeholder="First name..."
              className="name"
              value={form.firstName}
              onChange={updateForm('firstName')}
            ></input>
            <div id="firstName-msg" className="err-msg"></div>
          </div>

          <div className="section">
            <label htmlFor="name">Last name: </label>
            <input
              id="lastName"
              type="text"
              placeholder="Last name..."
              className="name"
              value={form.lastName}
              onChange={updateForm('lastName')}
            ></input>
            <div id="lastName-msg" className="err-msg"></div>
          </div>

          <div className="section">
            <label htmlFor="email">Email: </label>
            <input
              id="email"
              type="email"
              placeholder="Enter email..."
              className="email"
              value={form.email}
              onChange={updateForm('email')}
            ></input>
            <div id="email-msg" className="err-msg"></div>
          </div>

          <div className="section">
            <label htmlFor="username">Username: @</label>
            <input
              id="username"
              type="text"
              placeholder="Enter username..."
              className="username"
              value={form.username}
              onChange={updateForm('username')}
            ></input>
            <div id="username-msg" className="err-msg"></div>
          </div>

          <div className="section">
            <label htmlFor="password">Password: </label>
            <input
              id="password"
              type="password"
              placeholder="Enter password..."
              className="password"
              value={form.password}
              onChange={updateForm('password')}
            ></input>
            <div id="password-msg" className="err-msg"></div>
          </div>

          <div className="register-btn">
            <button type="submit" onClick={register}>Register</button>
          </div>
        </form>
        {/* </div> */}
      </div>
    </div>
  );
};

export default Register;

