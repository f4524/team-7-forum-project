/* eslint-disable max-len */
import {NavLink} from 'react-router-dom';
import './LogoutButton.css';
import {Avatar} from '@mui/material';
import AppContext from '../../data/app-state';
import {useContext} from 'react';

const LogoutButton = ({logout}) => {
  const {userData} = useContext(AppContext);

  const openMenu = () => {
    const menuBox = document.getElementById('menu-box');
    if (menuBox.style.display === 'block') {
      menuBox.style.display = 'none';
    } else {
      menuBox.style.display = 'block';
    }
  };

  return (
    <>
      <div className="drop-menu">
        <div className="profile-section">
          <button className="username-btn" onClick={openMenu}>
            <div className="profile">{userData.username}</div>
          </button>
          {(userData.avatarUrl !== undefined) ?
          <div className="avatar">
            <img src={userData.avatarUrl} alt="profile" className='imgProfile' onClick={openMenu}/>
          </div> :
          <div className="avatar">
            <Avatar
              onClick={openMenu}
              className="avatar-icon"
              sx={{bgcolor: '#f4976c', fontSize: 14, width: 80, height: 80}}
            />
          </div>
          }
        </div>

        <div id="menu-box">
          <div className="menu-content">
            <button
              onClick={() => {
                logout();
                openMenu();
              }}
              className="logout-btn"
            >
              Logout
            </button>
            <NavLink to="/profile" className="profile-btn" onClick={openMenu}>
              <div className="hide-menu-text">Profile</div>
            </NavLink>
            <NavLink to="/settings" className="settings-btn" onClick={openMenu}>
              <div className="hide-menu-text">Settings</div>
            </NavLink>
          </div>
        </div>
      </div>
    </>
  );
};

export default LogoutButton;
