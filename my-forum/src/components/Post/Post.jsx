/* eslint-disable max-len */
import {useContext} from 'react';
import AppContext from '../../data/app-state';
import {Avatar} from '@mui/material';
import {BsChatRightTextFill} from 'react-icons/bs';
import {NavLink} from 'react-router-dom';
import {AiOutlineEdit} from 'react-icons/ai';
import {FcLikePlaceholder} from 'react-icons/fc';
import {FcLike} from 'react-icons/fc';
import {MdDeleteOutline} from 'react-icons/md';

/**
 * @param {{ post: { id: string, content: string, author: string, date: Date, likedBy: string[] } } } post
 */

const Post = ({deletePost, post, like, dislike, setPost}) => {
  const {userData} = useContext(AppContext);

  const isPostLiked = () => post.likedBy.includes(userData.username);

  const handleLikeButtonClick = () =>
    isPostLiked() ? dislike(post) : like(post);

  return (
    <div className="post" key={post.id}>
      <div className="avatar">
        <Avatar
          className="avatar-icon"
          sx={{bgcolor: '#22c1c3', fontSize: 34, width: 80, height: 80}}/>
      </div>

      <div className="post-content">
        <div className="content">
          <div className="title">
            <div>
              <NavLink
                to={'/openpost'}
                onClick={() => setPost(post)}
                className="title-content">
                {post?.title}
              </NavLink>

              <div className="authorData">
                <label className="labelAuthor">author:</label> {post?.author}
              </div>
            </div>
            <div className="post-icon-home">
              <NavLink to={'/openpost'} onClick={() => setPost(post)}>
                <BsChatRightTextFill className="comment-icon" />
              </NavLink>

              {post.author === userData.username ? (
                <>
                  <AiOutlineEdit className="edit-icon" />
                  <MdDeleteOutline className="delete-icon" onClick={() => deletePost(post.id)}/>
                </>
              ) : null}
            </div>
          </div>
        </div>

        <div>
          <hr />
          <hr className="second-hr" />
        </div>

        <div className="right-section-post">
          <div className="post-like">
            <div className="post-like-nmb">
              <button onClick={handleLikeButtonClick}>
                {isPostLiked() ? (
                  <FcLike className="post-likes" />
                ) : (
                  <FcLikePlaceholder className="post-likes" />
                )}
              </button>
              <span>
                {post.likedBy.length} likes
                <lBsHandThumbsUp />
              </span>
            </div>
          </div>

          <div className="authorDateTime"> {post.date} </div>

        </div>
      </div>
    </div>
  );
};

export default Post;
