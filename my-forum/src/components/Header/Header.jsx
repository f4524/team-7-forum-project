import React from 'react';
import './Header.css';
import logo from '../../icons/logo.svg';
import NavBar from '../../views/NavBar/NavBar';
import Searchbar from '../../views/Searchbar/Searchbar';
import Logo from '../../views/Logo/Logo';
import LogInButtons from '../../views/LoginButton/LogInButtons';
import LogoutButton from '../../views/LogoutButton/LogoutButton';
import AppContext from '../../data/app-state';
import {useState, useContext, useEffect} from 'react';
import {useNavigate} from 'react-router-dom';
import {logoutUser} from '../../services/auth.service';

const Header = () => {
  const {user, setContext} = useContext(AppContext);
  const navigate = useNavigate();
  const [userD] = useState();

  useEffect(() => {
    // setUserD(getUserData(userData.id));
  }, []);

  const logout = () => {
    logoutUser()
        .then(() => {
          setContext({user: null, userData: null});
          navigate('/home');
        })
        .then(console.log('Logout'));
  };

  return (
    <div className="Header">
      <div className="left-section">
        <NavBar userD={userD} />
        <Searchbar />
      </div>

      <div className="logo-section">
        <Logo logo={logo} />
      </div>

      <div className="right-section">
        {user === null ? <LogInButtons /> : <LogoutButton logout={logout} />}
      </div>
    </div>
  );
};

export default Header;
