import React from 'react';
import {NavLink} from 'react-router-dom';
import './Footer.css';
import {BiListUl} from 'react-icons/bi';


const Footer = () => {
  return (
    <div className="Footer">
      <NavLink title="rules" to="/rules" className="rules-text">Rules</NavLink>
      <NavLink to="/rules" className="rules-icon"><BiListUl /> </NavLink >
    </div>
  );
};

export default Footer;
