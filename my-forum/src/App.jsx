/* eslint-disable max-len */
/* eslint-disable require-jsdoc */
import React from 'react';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import {useEffect, useState} from 'react';
import {Routes, Route, BrowserRouter, Navigate} from 'react-router-dom';
import SearchUser from './views/SearchUser/SearchUser';
import OpenPost from './views/OpenPost/OpenPost';
import Home from './views/Home/Home';
import About from './views/About/About';
import Contact from './views/Contact/Contact';
import Login from './views/Login/Login';
import Register from './views/Register/Register';
import Profile from './views/Profile/Profile';
import Rules from './components/Rules/Rules.jsx';
import AppContext from './data/app-state';
import AllPosts from './views/AllPosts/AllPosts';
import NewPosts from './views/NewPost/NewPost';
import Authenticated from './hoc/Authenticated';
import Search from './views/Search/Search';
import './App.css';
import {useAuthState} from 'react-firebase-hooks/auth';
import {auth} from './config/firebase-config';
import {getUserData} from './services/users.service';
import Settings from './views/Settings/Settings';

function App() {
  const [appState, setAppState] = useState({
    user: null,
    userData: null,
  });

  const [user] = useAuthState(auth);
  useEffect(() => {
    if (user === null) return;

    getUserData(user.uid)
        .then((snapshot) => {
          if (!snapshot.exists()) {
            throw new Error('Something went wrong!');
          }

          setAppState({
            user,
            userData: snapshot.val()[Object.keys(snapshot.val())[0]],
          });
        })
        .catch((e) => alert(e.message));
  }, [user]);

  return (
    <BrowserRouter>
      <AppContext.Provider value={{...appState, setContext: setAppState}}>
        <div className="App">
          <Header />

          <div className='body'>
            <Routes>
              <Route index element={<Navigate replace to="home" />} />
              <Route path="home" element={<Home/>} />
              <Route path="/login" element={<Login/>} />
              <Route path="/register" element={<Register/>} />
              <Route path="/profile" element={<Profile/>} />
              <Route path="/search" element={<Search />} />
              <Route path="logout" element={<Home />} />
              <Route path="all-posts" element={< Authenticated> <AllPosts /></Authenticated>} />
              <Route path="search-user" element={< Authenticated><SearchUser /></Authenticated>} />
              <Route path="rules-icon" element={<Rules />} />
              <Route path="/openpost" element={<Authenticated><OpenPost /></Authenticated>} />
              <Route path="newpost" element={<Authenticated><NewPosts/></Authenticated>} />
              <Route path="settings" element={<Authenticated><Settings /></Authenticated>} />
              <Route path="about" element={<About />} />
              <Route path="contact" element={<Contact />} />
              <Route path="rules" element={<Rules />} />
            </Routes>
          </div>
          <Footer />
        </div>
      </AppContext.Provider>
    </BrowserRouter>
  );
}

export default App;
