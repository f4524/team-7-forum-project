/* eslint-disable max-len */
import {
  ref,
  push,
  get,
  query,
  equalTo,
  orderByChild,
  update,
  onValue,
  set,
} from 'firebase/database';
import {db} from '../config/firebase-config';

export const fromCommentsDocument = (snapshot) => {
  const commentsDocument = snapshot.val(); // snapshot.val() -> {-N1DO6Xt27Hs5IAx9u8H: author: "gosho" content: "ne6to" createdOn: 1651656389813 likedBy: {gosho: true} [[Prototype]]: Object,      -N1DPbihp7GwRRfw8ZCM: {author: 'gosho',...}
  if (commentsDocument !== undefined && commentsDocument !== null) {
    return Object.keys(commentsDocument).map((key) => {
      const comment = commentsDocument[key];

      return {
        ...comment,
        id: key,
        createdOn: new Date(comment.createdOn),
        likedBy: comment.likedBy ? Object.keys(comment.likedBy) : [],
      };
    });
  }
};

export const addComment = (content, handle, postId) => {
  return push(
      ref(db, 'comments'),
      {
        content,
        author: handle,
        createdOn: Date.now(),
        postId: postId,
      }
  ).then((result) => {
    return getCommentById(result.key);
  });
};

export const getCommentById = (id) => {
  return get(ref(db, `comments/${id}`)).then((result) => {
    if (!result.exists()) {
      throw new Error(`Comment with id ${id} does not exist!`);
    }

    const comment = result.val();
    comment.id = id;
    comment.createdOn = new Date(comment.createdOn);
    if (!comment.likedBy) {
      comment.likedBy = [];
    } else {
      comment.likedBy = Object.keys(comment.likedBy);
    }

    return comment;
  });
};

export const getAllCommentsByPostId = (postId) => {
  return get(query(ref(db, 'comments'), orderByChild('postId'), equalTo(postId)))
      .then((snapshot) => {
        if (!snapshot.exists()) return [];

        return fromCommentsDocument(snapshot);
      });
};

export const getLikedComments = (handle) => {
  return get(ref(db, `users/${handle}`)).then((snapshot) => {
    if (!snapshot.val()) {
      throw new Error(`User with handle @${handle} does not exist!`);
    }

    const user = snapshot.val();
    if (!user.likedPosts) return [];

    return Promise.all(
        Object.keys(user.likedPosts).map((key) => {
          return get(ref(db, `comments/${key}`)).then((snapshot) => {
            const comment = snapshot.val();

            return {
              ...comment,
              createdOn: new Date(comment.createdOn),
              id: key,
              likedBy: comment.likedBy ? Object.keys(comment.likedBy) : [],
            };
          });
        })
    );
  });
};

export const getCommentsByAuthor = (handle) => {
  return get(
      query(ref(db, 'comments'), orderByChild('author'), equalTo(handle))
  ).then((snapshot) => {
    if (!snapshot.exists()) return [];

    return fromCommentsDocument(snapshot);
  });
};

export const getLiveComments = (listen) => {
  return onValue(ref(db, 'comments'), listen);
};

export const getAllComments = () => {
  return get(ref(db, 'comments')).then((snapshot) => {
    if (!snapshot.exists()) {
      return [];
    }

    return fromCommentsDocument(snapshot);
  });
};

export const likeComment = (handle, commentId) => {
  const updateLikes = {};
  updateLikes[`/comments/${commentId}/likedBy/${handle}`] = true;
  updateLikes[`/users/${handle}/likedComments/${commentId}`] = true;

  return update(ref(db), updateLikes);
};

export const dislikeComment = (handle, commentId) => {
  const updateLikes = {};
  updateLikes[`/comments/${commentId}/likedBy/${handle}`] = null;
  updateLikes[`/users/${handle}/likedComments/${commentId}`] = null;

  return update(ref(db), updateLikes);
};

export const deleteComment = async (id) => {
  const comment = await getCommentById(id);
  const updateLikes = {};

  comment.likedBy.forEach((handle) => {
    updateLikes[`/users/${handle}/likedComments/${id}`] = null;
  });

  await update(ref(db), updateLikes);

  return update(ref(db), {
    [`/comments/${id}`]: null,
  });
};

export const updateComment = async (author, content, commentId, postId, ) => {
  set(ref(db, 'comments/' + commentId), {
    author: author,
    content: content,
    createdOn: Date.now(),
    postId: postId,
  });
};
