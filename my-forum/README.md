## Description

I created a site to share personal experiences between complete strangers. There are various functionalities in the forum. A user may like or dislike posts, comment on other people's posts, and edit and delete their comments.
Posts that are written by a logged in user when pressing the correct buttons in the allpost the user can delete and update his post.
Create a search engine to search for user comments as well as search for tags with #.
My user should register and log in, as well as see and change their profile information.
When the administrator is logged in, he can block a user, which means that he will not be able to write comments and like posts and comments.
Rules for using the site have been created, which are currently partially implemented on our site.

## Start the project

<b>The backend</b> is a cloud backend *Firebase*. I give you a permission to see the real time database. 

https://console.firebase.google.com/u/0/project/forum-team7/database

<b>First step:</b> 

You should install Install Node.js and NPM on you machine

 https://radixweb.com/blog/installing-npm-and-nodejs-on-windows-and-mac


<b>Second step:</b>

You should run <b>*npm i / npm install*</b> as a command in the terminal to install all the packages that the project uses.

<b>Third step:</b>

Run <b>*npm start*</b> to start the project


## Support

<b>Register</b> -- You can register on the site.
<img src='./imgReadme/register.PNG'>

<b>Login</b> -- Log in to your profile
<img src='./imgReadme/login.PNG'>

<b>Home</b> -- This is the homepage
<img src='./imgReadme/home.PNG'>

<b>About</b> -- Information about the forum
<img src='./imgReadme/about.PNG'>

<b>Contact</b> --  My contact information
<img src='./imgReadme/contact.PNG'>

<b>AllPost</b> -- This page contains all posts posted in the forum
<img src='./imgReadme/allpost.PNG'>


<b>Adminusersearch</b> -- Only the administrator has access to this page
<img src='./imgReadme/adminusersearch.PNG'>

<b>CreatePost</b> -- Create your post
<img src='./imgReadme/createpost.PNG'>

<b>OpenPost</b> -- Open a post, read and write your opinion as a comment
<img src='./imgReadme/openpost.PNG'>

<b>Rules</b> -- Rules for using the forum 
<img src='./imgReadme/rules.PNG'>

<b>SearchUser</b> -- Search posts by user
<img src='./imgReadme/searchuser.PNG'>

<b>Settings</b> -- Here you can change your personal information
<img src='./imgReadme/settings.PNG'>

<b>Profile</b> -- Here you can view your personal information
<img src='./imgReadme/profile.PNG'>

